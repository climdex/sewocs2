<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib prefix="s" uri="/struts-tags"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<!--
/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. 
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 	
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.

 * CLIMDEX Project Group provides access to this code for bona fide
 * scientific research and personal usage only.

 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow 
 * researchers. Everyone is welcome to use the code, and we aim to make it 
 * reliable, but it does not have the same level of support as an operational 
 * service.
 */
-->
<head>
<meta http-equiv="content-type"
	content="application/xhtml+xml; charset=UTF-8" />
<title>STATDEX on Open Street Map</title>
<meta name="description" content="STATDEX on Open Street Map" />
<meta name="keywords"
	content="sewocs,climdex,STATDEX,GHCN-Daily Stations,extremes,climate,climate extremes,climate indices,precipitation,temparature,etccdi,climate change detection" />

<s:head />

<!-- pageContext.request.contextPath is needed because this page is <object> and used in IE -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/styles/style.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/openlayers/theme/default/style.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/openlayers/theme/default/scalebar-fat.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/styles/map-jsp.css"
	type="text/css" />

<script
	src="${pageContext.request.contextPath}/openlayers/OpenLayers.js"
	type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/scripts/map.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/scripts/checkDataSource.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/scripts/checkSeason.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/scripts/checkYears.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/scripts/checkSubmit.js"
	type="text/javascript"></script>

<script type="text/javascript">
	/* This script contains Struts2 action scope varaible reference. 
	It does not work in script files but it works on JSP pages */
	var startYearJSArrayStr = '${datasetOptions.startYearJSArray}';
	var endYearJSArrayStr = '${datasetOptions.endYearJSArray}';
	var layerNameJSArrayStr = '${datasetOptions.layerNameJSArray}';
	var styleNameJSArrayStr = '${datasetOptions.styleNameJSArray}';
	var startYearJSArrayStrEvalStr = 'var startYearJSArray = '
			+ startYearJSArrayStr + ";";
	var endYearJSArrayStrEvalStr = 'var endYearJSArray = ' + endYearJSArrayStr
			+ ";";
	var layerNameJSArrayStrEvalStr = 'var layerNameJSArray = '
			+ layerNameJSArrayStr + ";";
	var styleNameJSArrayStrEvalStr = 'var styleNameJSArray = '
			+ styleNameJSArrayStr + ";";

	eval(startYearJSArrayStrEvalStr);
	eval(endYearJSArrayStrEvalStr);
	eval(layerNameJSArrayStrEvalStr);
	eval(styleNameJSArrayStrEvalStr);
	; // don't remove the semicolon on the left. it is needed.
</script>

<script type="text/javascript">
	var previousDataset = "";
	/*
	 * init() function initialises the map with the default dataset, set in the request.
	 * It also initialises the previosDataset as the currentDataset.
	 */
	function init()
	{
		var currentDataset = '${stationdexRequest.dataSource}';
		//changeDatasetList(currentDataset);
		if (previousDataset == "") previousDataset = currentDataset;
		changeStartEndYears(previousDataset, currentDataset);
		previousDataset = currentDataset;
		var mapCenter = '${stationdexRequest.mapCenter}';
		var mapZoom = '${stationdexRequest.mapZoom}';
		createMap(mapCenter, mapZoom, currentDataset);
	}

	function clearErrMessage()
	{
		var errorMsgElement = document.getElementById("errorMsg");
		// errorMsgElement may not exist when there was no error before
		if (errorMsgElement) 
		{
			errorMsgElement.innerHTML = "";
		}
	}
</script>

</head>
<body onload="init();" style="background: #F2F2F2; padding: 0 0 0 0; margin: 0 0 0 0; width: 1022px">

	<div id="station_form"
		style="position: relative; background: lightgrey">
		<s:form id="myForm" name="mayForm" method="get" theme="simple"
			action="stationdex" validate="false"
			onsubmit="return checkSubmit(this)" accept-charset="UTF-8">
			<fieldset id="fields">
				<div id="dataset" class="select">
					(1) Select data source to choose a station from.
					<div class="field">
						<s:label key="label.stationdexRequest.dataSource" />
						<s:select key="stationdexRequest.dataSource"
							list="stationdexOptions.dataSourceList" listKey="key"
							listValue="value"
							onchange="clearErrMessage();return checkDataSource(this)" />
					</div>
				</div>

				<div id="mapinstruction" class="select" style="background: lightgrey;">
					<p>(2) Select a station from the map below.</p>
					<div id="maptips" style="position: relative; left: 30px">
						<ul>
							<li>Zoom-in: Use pan or double click, or shift + drag mouse (with left button down)</li>
							<li>Select: Click on a dot on the map to select a station.</li>
						</ul>
					</div>
				</div>

				<div id="maparea"
					style="clear: both; position: relative; float: left;">

					<div>
						<div id="map" class="smallmap"></div>
						<div id="wrapper1">
							<!--<div id="scalebar"></div>-->
							<!--<div id="scale"></div>-->
							<div id="mouseloc">longitude, latitude</div>
						</div>
					</div>
				</div>
				<div id="station" style="position: static">
					<div class="select" style="position: static; float: left; background: lightgrey ! important">
						(3) Selected station information will show below.
						<div class="field">
							<s:label key="label.stationdexRequest.stationId" />
							<s:textfield key="stationdexRequest.stationId" readonly="true" />
						</div>
						<div class="field">
							<s:label key="label.stationdexRequest.stationName" />
							<s:textfield key="stationdexRequest.stationName" readonly="true" />
						</div>
						<div class="field">
							<s:label key="label.stationdexRequest.latitude" />
							<s:textfield key="stationdexRequest.latitude" readonly="true" />
						</div>
						<div class="field">
							<s:label key="label.stationdexRequest.longitude" />
							<s:textfield key="stationdexRequest.longitude" readonly="true" />
						</div>
						<div class="field">
							<s:label key="label.stationdexRequest.altitude" />
							<s:textfield key="stationdexRequest.altitude" readonly="true" />
						</div>
						<div class="field">
							<s:label key="label.stationdexRequest.active" />
							<s:textfield key="stationdexRequest.active" readonly="true" />
						</div>
						<div class="field">
							<!--<s:label key="label.stationdexRequest.mapCenter" />-->
							<s:hidden key="stationdexRequest.mapCenter" />
						</div>
						<div class="field">
							<!--<s:label key="label.stationdexRequest.mapZoom" />-->
							<s:hidden key="stationdexRequest.mapZoom" />
						</div>
					</div>
					<div id="params" class="select"
						style="position: static; float: left">
						(4) Select parameters to get the plot or data file for the
						station.
						<div class="field">
							<s:label key="label.stationdexRequest.climateIndex" />
							<s:select key="stationdexRequest.climateIndex"
								list="stationdexOptions.climateIndexList" listKey="key"
								listValue="value"
								onchange="clearErrMessage();return checkSeasonType(this)" />
						</div>
						<div class="field">
							<s:label key="label.stationdexRequest.startYear" />
							<s:select key="stationdexRequest.startYear"
								list="stationdexOptions.startYearList"
								onchange="clearErrMessage();return checkEndYear(this)" />
						</div>
						<div class="field">
							<s:label key="label.stationdexRequest.endYear" />
							<s:select key="stationdexRequest.endYear"
								list="stationdexOptions.endYearList"
								onchange="clearErrMessage();return checkStartYear(this)" />
						</div>
						<div class="field">
							<s:label key="label.stationdexRequest.season" />
							<s:select key="stationdexRequest.season"
								list="stationdexOptions.seasonList" listKey="key"
								listValue="value" onchange="clearErrMessage();return true;" />
						</div>
						<div class="field">
							<s:label key="label.stationdexRequest.outputType" />
							<s:select key="stationdexRequest.outputType"
								list="stationdexOptions.outputTypeList" listKey="key"
								listValue="value" onchange="clearErrMessage();return true" />
						</div>
					</div>

					<div id="submit" class="select"
						style="position: static; float: left">
						(5) When your selections are correct, press OK.
						<div>
							<s:submit value="OK" onclick="clearErrMessage();return true" />
						</div>
					</div>
				</div>
				
				<s:if test="hasActionErrors()">
						<s:actionerror id="errorMsg" cssStyle="padding-left: 10px;" />
				</s:if>
			</fieldset>
		</s:form>
	</div>

	<div id="note"
		style="clear: both; padding-top: 5px; padding-bottom: 10px; padding-left: 10px">
		<p>
			<!-- TODO: This message should change, depending on the data set and the date of the source data. -->
			<b>*</b> Station locations are based on the data provided to us. Please note that station coordinates may
			not be as accurate as the map precision.
		</p>
	</div>

	<!-- This item cannot be removed. It used by clickHandler() in map.js -->
	<div id="loadingMsg"></div>

</body>
</html>
