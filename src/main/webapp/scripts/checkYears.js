/*
 * checkYears.js
 */

function checkEndYear(startYearSelect)
{
	// clearErrorMsg();
	var startYearIndex = startYearSelect.selectedIndex;
	var endYearSelect = document
			.getElementById("myForm_stationdexRequest_endYear");
	;
	var endYearIndex = endYearSelect.selectedIndex;
	// alert("start="+startYearIndex+" end="+endYearIndex);
	if (startYearIndex > endYearIndex)
	{
		alert("Start Year must be the same or earlier than End Year.");
		endYearSelect.selectedIndex = startYearIndex;
	}
	return true;
}

function checkStartYear(endYearSelect)
{
	// clearErrorMsg();
	var endYearIndex = endYearSelect.selectedIndex;
	var startYearSelect = document
			.getElementById("myForm_stationdexRequest_startYear");
	var startYearIndex = startYearSelect.selectedIndex;
	// alert("start="+startYearIndex+" end="+endYearIndex);
	if (startYearIndex > endYearIndex)
	{
		alert("End Year must be the same or later than Start Year");
		startYearSelect.selectedIndex = endYearIndex;
	}
	return true;
}

/*
 * If the oldDataset == newDataset, it just sets startYearIndex=0 and
 * endYearIndex=length-1.
 * 
 * It also preserves the previously selected startYear and endYear if they are
 * within the rage of the new dataSet.
 */
function changeStartEndYears(oldDataSet, newDataset)
{
	var startYearSelectId = "myForm_stationdexRequest_startYear";
	var startYearSelect = document.getElementById(startYearSelectId);
	var endYearSelectId = "myForm_stationdexRequest_endYear";
	var endYearSelect = document.getElementById(endYearSelectId);

	var currentFromYearIndex = startYearSelect.selectedIndex;
	var currentToYearIndex = endYearSelect.selectedIndex;

	var currentFromYear = "";
	var currentToYear = "";
	if (typeof (currentFromYearIndex) != "undefined")
	{
		currentFromYear = startYearSelect.options[currentFromYearIndex].value;
		currentToYear = endYearSelect.options[currentToYearIndex].value;
	}

	var newStartYear = Number(startYearJSArray[newDataset]);
	var newEndYear = Number(endYearJSArray[newDataset]);
	var numOfYears = newEndYear - newStartYear + 1;

	startYearSelect.options.length = 0;
	endYearSelect.options.length = 0;
	for ( var i = 0; i < numOfYears; i++)
	{
		var yearString = new String(i+newStartYear);
		startYearSelect.options[i] = new Option(yearString, yearString, false,
				false);
		endYearSelect.options[i] = new Option(yearString, yearString, false,
				false);
	}

	if (typeof (currentFromYearIndex) == "undefined")
	{
		startYearSelect.selectedIndex = 0;
		endYearSelect.selectedIndex = numOfYears -1;
		return;
	}

	var oldStartYear = startYearJSArray[oldDataSet];
	var oldEndYear = endYearJSArray[oldDataSet];
	var startYearOffset = newStartYear - oldStartYear;

	var newStartYearIndex = 0;
	var newEndYearIndex = numOfYears - 1;

	if (currentFromYear < newStartYear)
		newStartYearIndex = 0;
	else
		if (newEndYear < currentFromYear)
			newStartYearIndex = numOfYears - 1;
		else
			newStartYearIndex = currentFromYearIndex - startYearOffset;

	if (currentToYear < newStartYear)
		newEndYearIndex = 0;
	else
		if (newEndYear < currentToYear)
			newEndYearIndex = numOfYears - 1;
		else
			newEndYearIndex = currentToYearIndex - startYearOffset;

	startYearSelect.selectedIndex = newStartYearIndex;
	endYearSelect.selectedIndex = newEndYearIndex;
}