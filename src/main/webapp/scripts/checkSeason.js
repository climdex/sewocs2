/* season related scripts */

// TODO: This is defined by Seasonality.class
var indexToSeasonalList = new Array();
indexToSeasonalList = {
	"FD" : "annualOnly",
	"SU" : "annualOnly",
	"ID" : "annualOnly",
	"TR" : "annualOnly",
	"GSL" : "annualOnly",
	"TXx" : "seasonable",
	"TNx" : "seasonable",
	"TXn" : "seasonable",
	"TNn" : "seasonable",
	"TN10p" : "seasonable",
	"TX10p" : "seasonable",
	"TN90p" : "seasonable",
	"TX90p" : "seasonable",
	"WSDI" : "annualOnly",
	"CSDI" : "annualOnly",
	"DTR" : "seasonable",
	"Rx1day" : "seasonable",
	"Rx5day" : "seasonable",
	"SDII" : "annualOnly",
	"R10mm" : "annualOnly",
	"R20mm" : "annualOnly",
	"Rnn" : "annualOnly",
	"CDD" : "annualOnly",
	"CWD" : "annualOnly",
	"R95p" : "annualOnly",
	"R99p" : "annualOnly",
	"PRCPTOT" : "annualOnly"
};

function checkSeasonType(indexSelect)
{
	var selectedIndex = indexSelect.selectedIndex;
	var indexValue = indexSelect.options[selectedIndex].value;
	var seasonSelect = document
			.getElementById("myForm_stationdexRequest_season");
	var seasonability = indexToSeasonalList[indexValue];
	if (seasonability == "annualOnly")
	{
		seasonSelect.options.length = 0;
		seasonSelect.options.add(new Option("Annual", "ANN", true, false));
		seasonSelect.value = "ANN";
	} else
	{
		seasonSelect.options.length = 0;
		seasonSelect.options[0] = new Option("Annual", "ANN", true, false);
		// seasonSelect.options[1] = new Option("DJF", "DJF", false, false);
		// seasonSelect.options[2] = new Option("MAM", "MAM", false, false);
		// seasonSelect.options[3] = new Option("JJA", "JJA", false, false);
		// seasonSelect.options[4] = new Option("SON", "SON", false, false);
		seasonSelect.options[1] = new Option("January", "JAN", false, false);
		seasonSelect.options[2] = new Option("February", "FEB", false, false);
		seasonSelect.options[3] = new Option("March", "MAR", false, false);
		seasonSelect.options[4] = new Option("April", "APR", false, false);
		seasonSelect.options[5] = new Option("May", "MAY", false, false);
		seasonSelect.options[6] = new Option("June", "JUN", false, false);
		seasonSelect.options[7] = new Option("July", "JUL", false, false);
		seasonSelect.options[8] = new Option("August", "AUG", false, false);
		seasonSelect.options[9] = new Option("September", "SEP", false, false);
		seasonSelect.options[10] = new Option("October", "OCT", false, false);
		seasonSelect.options[11] = new Option("November", "NOV", false, false);
		seasonSelect.options[12] = new Option("December", "DEC", false, false);
	}
	return true;
}

function changeSeasonsForHADEX2()
{
	// TODO: Currently this does nothing
	// It has to check climateIndex and dataSource to determine proper
	// values
}

function changeSeasonsForGHCNDEX()
{
	// TODO: Currently this does nothing
	// It has to check climateIndex and dataSource to determine proper
	// values
}