function checkSubmit(theForm)
{
	// var endYearSelect = theForm.elements["stationIndexRequest.endYear"];
	// var endYearIndex = endYearSelect.selectedIndex;
	// var startYearSelect = theForm.elements["stationIndexRequest.startYear"];
	// var startYearIndex = startYearSelect.selectedIndex;
	// var yearDiff = endYearIndex - startYearIndex;
	// if (yearDiff < 9)
	// {
	// alert("Time series is not calculated for less than 10 years.");
	// return false;
	// }
	// else
	// return true;
	var stationId = theForm.elements["stationdexRequest.stationId"].value;
	if (stationId != null && stationId == "")
	{
		alert("Please select a station");
		return false;
	}
	var climateIndex = theForm.elements["stationdexRequest.climateIndex"].value;
	if (climateIndex == "")
	{
		alert("Please select an climate index");
		return false;
	}
	var active = theForm.elements["stationdexRequest.active"].value;
	if (active != "Yes")
	{
		alert("Station data is not authorised for distribution");
		return false;
	}
	return true;
}