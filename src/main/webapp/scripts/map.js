/**
 * map.js Copyright CLIMDEX 2011-2012
 * 
 * Main part of this code follows the google-v3.html example provided by
 * OpenLayers 2.10 package. Some part of the code was taken from the
 * spherical-mercator.html example.
 * 
 * The code for Controls was found elsewhere on the Web. scalebar-fat.css was
 * taken from the same Web site.
 * 
 * The Feature request code was taken from the GeoServer LayerPreview GUI.
 * 
 * @see http://trac.osgeo.org/openlayers/wiki/GetFeatureInfo
 * 
 * TODO: Change the Layers to be tiled and using a buffer.
 */

"user strict";

var map;
// pink tile avoidance
OpenLayers.IMAGE_RELOAD_ATTEMPTS = 5;
// make OL compute scale according to WMS spec
OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;

// This stores the current statinos layer
var currentStationsLayer = "";
// This reference is needed to swap stations layer
var clickControl = "";

function createMap(mapCenter, mapZoom, dataSet)
{
	var geographic = new OpenLayers.Projection("EPSG:4326");
	var mercator = new OpenLayers.Projection("EPSG:900913");

	var world = new OpenLayers.Bounds(-180, -89, 180, 89).transform(geographic,
			mercator);

	var overviewMapCtl = new OpenLayers.Control.OverviewMap();
	var scaleLineCtl = new OpenLayers.Control.ScaleLine();
	var scaleCtl = new OpenLayers.Control.Scale();
	var mouseLocDiv = document.getElementById("mouseloc");
	var mouseLocCtl = new OpenLayers.Control.MousePosition(
	{
		'div' : mouseLocDiv,
		'autoActivate' : true
	});
	var navCtl = new OpenLayers.Control.Navigation();
	var panZoomBarCtl = new OpenLayers.Control.PanZoomBar();
	panZoomBarCtl.zoomWorldIcon = true;
	panZoomBarCtl.zoomStopHeight = 10;
	var keybCtl = new OpenLayers.Control.KeyboardDefaults();

	var mapOptions =
	{
		projection : mercator,
		displayProjection : geographic,
		units : "m",
		numZoomLevels : 10,
		maxResolution : "auto",
		minResolution : "auto",
		maxExtent : world,
		controls : [ overviewMapCtl, scaleLineCtl, scaleCtl, mouseLocCtl,
		             navCtl, panZoomBarCtl, keybCtl ]
	};

	map = new OpenLayers.Map('map', mapOptions);

	var baseLayer = new OpenLayers.Layer.OSM();

	// this is used to swap station layers
	currentStationsLayer = getStationsLayer(dataSet);

	map.addLayers([ baseLayer, currentStationsLayer ]);

	clickControl = getClickControl(currentStationsLayer);
	
	map.addControl(clickControl);
	clickControl.activate();

	// mapCenter and mapZoom are global properties
	var centerLonLat = OpenLayers.LonLat.fromString(mapCenter);
	if (mapCenter != "" && mapZoom != "")
	{
		map.setCenter(centerLonLat, mapZoom);
	}
	else
		if (!map.getCenter())
		{
			map.zoomToMaxExtent();
		}
}

function getStationsLayer(dataSet)
{
	var layerId = layerNameJSArray[dataSet];
	var layerStyle = styleNameJSArray[dataSet];

	var protocol = window.location.protocol;
	var host = window.location.host;
	var wmsUrl = protocol + "//" + host + "/geoserver/wms";
	var gwcUrl = protocol + "//" + host + "/geoserver/gwc/service/wms";

	var layerParams =
	{
		'layers' : layerId,
		layerUrls : [ gwcUrl ],
		'STYLES' : layerStyle,
		'transparent' : true
	};
	var layerOptions =
	{
		visibility : true,
		'isBaseLayer' : false,
		'wrapDateLine' : false
	};
	var stationsLayer = new OpenLayers.Layer.WMS(dataSet, wmsUrl, layerParams,
			layerOptions);

	return stationsLayer;
}

function getClickControl(stationsLayer)
{
	var protocol = window.location.protocol;
	var host = window.location.host;
	
	var wmsUrl = protocol + "//" + host + "/geoserver/wms";
	var gwcUrl = protocol + "//" + host + "/geoserver/gwc/service/wms";

	var gmlFormat = new OpenLayers.Format.GML();
	var clickControl = new OpenLayers.Control.WMSGetFeatureInfo(
	{
		url : wmsUrl,
		layerUrls : [ gwcUrl ],
		title : 'selected station',
		maxFeatures : 1,
		layers : [ stationsLayer ],
		styles : [ "selected_stations" ],
		infoFormat : "application/vnd.ogc.gml",
		format : gmlFormat,
		queryVisible : true
	});

	clickControl.events.register("getfeatureinfo", this, showInfo);

	return clickControl;
}

/**
 * showInfo is called by the 'click' Control. It sets the HTML with returned
 * Attribute values.
 */
function showInfo(evt)
{
	var features = evt.features;

	if (features && features.length)
	{
		// highlightLayer.destroyFeatures();
		// highlightLayer.addFeatures(features);
		// highlightLayer.redraw();
		// expeting only 1 feature
		var feature = features[0];
		showAttributes(feature);
	}
	else
	{
		showAttributes(null);
	}
}

function showAttributes(feature)
{
	var attributes = (feature) ? feature.attributes : null;

	var stationId = (feature) ? attributes.ID : "";
	var stationName = (feature) ? attributes.NAME : "";
	var latitude = (feature) ? attributes.LATITUDE : "";
	var longitude = (feature) ? attributes.LONGITUDE : "";
	var altitude = (feature) ? attributes.ALTITUDE : "";
	var active = (feature) ? attributes.ACTIVE : "";

	document.getElementById("myForm_stationdexRequest_stationId").value = stationId;
	document.getElementById("myForm_stationdexRequest_stationName").value = stationName;
	document.getElementById("myForm_stationdexRequest_latitude").value = latitude;
	document.getElementById("myForm_stationdexRequest_longitude").value = longitude;
	document.getElementById("myForm_stationdexRequest_altitude").value = altitude;
	var openData = "";
	if (active)
	{
		openData = (active == "1") ? "Yes" : "No";
	}

	document.getElementById("myForm_stationdexRequest_active").value = openData;

	// hidden element
	document.getElementById("myForm_stationdexRequest_mapCenter").value = map
			.getCenter().toShortString();
	// hidden element
	document.getElementById("myForm_stationdexRequest_mapZoom").value = map
			.getZoom();
}

function switchMapOverlay(newDataset)
{
	map.removeControl(clickControl);
	var setNewBaseLayer = false;
	map.removeLayer(currentStationsLayer, setNewBaseLayer);

	currentStationsLayer = getStationsLayer(newDataset);
	map.addLayer(currentStationsLayer);
	clickControl = getClickControl(currentStationsLayer);	
	map.addControl(clickControl);
	clickControl.activate();
}