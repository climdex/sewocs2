/*
 * checkDataSource.js
 * 
 * previousDataset is defined and initialised by map.jsp as a global variable.
 */
function checkDataSource(dataSourceSelect)
{
	var selectedIndex = dataSourceSelect.selectedIndex;
	var newDataset = dataSourceSelect.options[selectedIndex].value;
	var oldDataset = previousDataset;
	changeStartEndYears(oldDataset, newDataset);
	switchMapOverlay(newDataset);
	previousDataset = newDataset;
	return true;
}