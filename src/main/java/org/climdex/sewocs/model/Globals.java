package org.climdex.sewocs.model;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Default values for StationdexRequest
 * Default values are the initial values for a stationdexRequest
 * These can be defined and overridden by applicationContext.xml
 * Users change the actual values on the JSP page.
 * @author yoichi
 *
 */
public class Globals
{
	@Autowired
	public static  String DEFAULT_DATA_SOURCE = "GHCND";
	@Autowired
	public static  String DEFAULT_CLIMATE_INDEX = "TXx";
	@Autowired
	@Deprecated // but this is still required
	public static  int DEFAULT_START_YEAR = 1851;
	@Autowired
	@Deprecated // but this is still required
	public static  int DEFAULT_END_YEAR = Calendar.getInstance().get(Calendar.YEAR); 
	@Autowired
	public static  String DEFAULT_SEASON = "ANN";
	@Autowired
	public static  String DEFAULT_OUTPUT_TYPE = "Plot";
	
	public static String getDEFAULT_DATA_SOURCE()	
	{
		return DEFAULT_DATA_SOURCE;
	}
	public static void setDEFAULT_DATA_SOURCE(String dEFAULT_DATA_SOURCE)
	{
		DEFAULT_DATA_SOURCE = dEFAULT_DATA_SOURCE;
	}
	public static String getDEFAULT_CLIMATE_INDEX()
	{
		return DEFAULT_CLIMATE_INDEX;
	}
	public static void setDEFAULT_CLIMATE_INDEX(String dEFAULT_CLIMATE_INDEX)
	{
		DEFAULT_CLIMATE_INDEX = dEFAULT_CLIMATE_INDEX;
	}
	@Deprecated
	public static int getDEFAULT_START_YEAR()
	{
		return DEFAULT_START_YEAR;
	}
	@Deprecated
	public static void setDEFAULT_START_YEAR(int dEFAULT_START_YEAR)
	{
		DEFAULT_START_YEAR = dEFAULT_START_YEAR;
	}
	@Deprecated
	public static int getDEFAULT_END_YEAR()
	{
		return DEFAULT_END_YEAR;
	}
	@Deprecated
	public static void setDEFAULT_END_YEAR(int dEFAULT_END_YEAR)
	{
		DEFAULT_END_YEAR = dEFAULT_END_YEAR;
	}
	public static String getDEFAULT_SEASON()
	{
		return DEFAULT_SEASON;
	}
	public static void setDEFAULT_SEASON(String dEFAULT_SEASON)
	{
		DEFAULT_SEASON = dEFAULT_SEASON;
	}
	public static String getDEFAULT_OUTPUT_TYPE()
	{
		return DEFAULT_OUTPUT_TYPE;
	}
	public static void setDEFAULT_OUTPUT_TYPE(String dEFAULT_OUTPUT_TYPE)
	{
		DEFAULT_OUTPUT_TYPE = dEFAULT_OUTPUT_TYPE;
	}
}
