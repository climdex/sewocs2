package org.climdex.sewocs.model;

public class StationdexRequest
{
	private String stationId = null;
	@Deprecated
	private String countryId = null;
	private String stationName = null;
	private String latitude = null;
	private String longitude = null;
	private String altitude = null;
	private String active = null;
	@Deprecated
	private String usState = null;
	@Deprecated
	private String gsn = null;
	@Deprecated
	private String hcn = null;
	@Deprecated
	private String wmoid = null;
	private String dataSource = "GHCND";
	private String climateIndex = "TXx";
	private String startYear = "1851"; // this will be overridden by datasetOptions
	private String endYear = "2013"; // this will be overridden by datasetOptions
	private String season = "ANN";
	private String outputType = "Plot";
	/**
	 * empty mapCenter or mapZoom will get map.zoomToMaxExtent().
	 * 
	 * var centerlonLat = centerLonLatOpenLayers.LonLat.fromString(mapCenter);
	 */
	private String mapCenter = "";
	/**
	 * empty mapCenter or mapZoom will get map.zoomToMaxExtent()
	 */
	private String mapZoom = "";

	public String getStationId()
	{
		return stationId;
	}

	public void setStationId(String stationId)
	{
		this.stationId = stationId;
	}

	@Deprecated
	public String getCountryId()
	{
		return countryId;
	}

	@Deprecated
	public void setCountryId(String countryId)
	{
		this.countryId = countryId;
	}

	public String getStationName()
	{
		return stationName;
	}

	public void setStationName(String stationName)
	{
		this.stationName = stationName;
	}

	public String getLatitude()
	{
		return latitude;
	}

	public void setLatitude(String latitude)
	{
		this.latitude = latitude;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public String getAltitude()
	{
		return altitude;
	}

	public void setAltitude(String altitude)
	{
		this.altitude = altitude;
	}

	public String getActive()
	{
		return active;
	}

	public void setActive(String active)
	{
		this.active = active;
	}

	@Deprecated
	public String getUsState()
	{
		return usState;
	}

	@Deprecated
	public void setUsState(String usState)
	{
		this.usState = usState;
	}

	@Deprecated
	public String getGsn()
	{
		return gsn;
	}

	@Deprecated
	public void setGsn(String gsn)
	{
		this.gsn = gsn;
	}

	@Deprecated
	public String getHcn()
	{
		return hcn;
	}

	@Deprecated
	public void setHcn(String hcn)
	{
		this.hcn = hcn;
	}

	@Deprecated
	public String getWmoid()
	{
		return wmoid;
	}

	@Deprecated
	public void setWmoid(String wmoid)
	{
		this.wmoid = wmoid;
	}

	public String getDataSource()
	{
		return dataSource;
	}

	public void setDataSource(String dataSource)
	{
		this.dataSource = dataSource;
	}

	public String getClimateIndex()
	{
		return climateIndex;
	}

	public void setClimateIndex(String climateIndex)
	{
		this.climateIndex = climateIndex;
	}

	public String getStartYear()
	{
		return startYear;
	}

	public void setStartYear(String startYear)
	{
		this.startYear = startYear;
	}

	public String getEndYear()
	{
		return endYear;
	}

	public void setEndYear(String endYear)
	{
		this.endYear = endYear;
	}

	public String getSeason()
	{
		return season;
	}

	public void setSeason(String season)
	{
		this.season = season;
	}

	public String getOutputType()
	{
		return outputType;
	}

	public void setOutputType(String outputType)
	{
		this.outputType = outputType;
	}

	public String getMapCenter()
	{
		return mapCenter;
	}

	public void setMapCenter(String mapCenter)
	{
		this.mapCenter = mapCenter;
	}

	public String getMapZoom()
	{
		return mapZoom;
	}

	public void setMapZoom(String mapZoom)
	{
		this.mapZoom = mapZoom;
	}
}
