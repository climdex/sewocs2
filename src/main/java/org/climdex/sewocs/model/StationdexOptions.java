/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.sewocs.model;

import java.util.ArrayList;
import java.util.List;

/**
 * StationdexRequest options for the HTML FORM SELECT These are default options
 * Actual options values would change on the actual JSP.
 * 
 * Values for dataSourceList, startYearList and endYearList are now reset by
 * DatasetOptions on the JSP page, although this class still needs to provide
 * the lists for JSP to draw the page initially.
 * 
 * @author yoichi
 * 
 */
public class StationdexOptions
{
	private List<KeyValueElement> dataSourceList;
	private List<KeyValueElement> climateIndexList;
	private List<String> startYearList;
	private List<String> endYearList;
	@Deprecated
	private int yearListStart = Globals.DEFAULT_START_YEAR;
	/** This is for GHCND but needs to be that of the current DataSet */
	@Deprecated
	private int yearListEnd = Globals.DEFAULT_END_YEAR;
	private List<KeyValueElement> seasonList;
	private List<KeyValueElement> outputTypeList;

	/**
	 * Constructor with initializing values TODO: These can be done as
	 * initializers
	 */
	public StationdexOptions()
	{
		initDataSourceList(); // values will be overridden
		initClimateIndexList();
		initStartYearList(); // values will be overridden
		initEndYearList(); // values will be overridden
		initSeasonList();
		initOutputTypeList();
	}

	private void initDataSourceList()
	{
		dataSourceList = new ArrayList<KeyValueElement>();

		dataSourceList.add(new KeyValueElement("GHCND", "GHCND Stations"));
		// dataSetList.add(new KeyValueElement("HADEX1", "HADEX1"));
		dataSourceList.add(new KeyValueElement("HADEX2", "HADEX2 Stations"));
	}

	private void initClimateIndexList()
	{
		climateIndexList = new ArrayList<KeyValueElement>();
		climateIndexList.add(new KeyValueElement("FD", "Frost days (FD)"));
		climateIndexList.add(new KeyValueElement("SU", "Summer days (SU)"));
		climateIndexList.add(new KeyValueElement("ID", "Ice days (ID)"));
		climateIndexList.add(new KeyValueElement("TR", "Tropical nights (TR)"));
		climateIndexList.add(new KeyValueElement("GSL",
				"Growing season length (GSL)"));
		climateIndexList.add(new KeyValueElement("TXx", "Max Tmax (TXx)"));
		climateIndexList.add(new KeyValueElement("TNx", "Max Tmin (TNx)"));
		climateIndexList.add(new KeyValueElement("TXn", "Min Tmax (TXn)"));
		climateIndexList.add(new KeyValueElement("TNn", "Min Tmin (TNn)"));
		climateIndexList
				.add(new KeyValueElement("TN10p", "Cool nights (TN10p)"));
		climateIndexList.add(new KeyValueElement("TX10p", "Cool days (TX10p)"));
		climateIndexList
				.add(new KeyValueElement("TN90p", "Warm nights (TN90p)"));
		climateIndexList.add(new KeyValueElement("TX90p", "Warm days (TX90p)"));
		climateIndexList.add(new KeyValueElement("WSDI",
				"Warm spell duration indicator (WSDI)"));
		climateIndexList.add(new KeyValueElement("CSDI",
				"Cold spell duration indicator (CSDI)"));
		climateIndexList.add(new KeyValueElement("DTR",
				"Diurnal temparature range (DTR)"));
		climateIndexList.add(new KeyValueElement("Rx1day",
				"Max 1-day precipitation amount (Rx1day)"));
		climateIndexList.add(new KeyValueElement("Rx5day",
				"Max 5-day precipitation amount (Rx5day)"));
		climateIndexList.add(new KeyValueElement("SDII",
				"Simple daily intensity index (SDII)"));
		climateIndexList.add(new KeyValueElement("R10mm",
				"Number of heavy precipitation days (R10mm)"));
		climateIndexList.add(new KeyValueElement("R20mm",
				"Number of very heavy precipitation days (R20mm)"));
		// climateIndexList.add(new KeyValueElement("Rnnmm",
		// "Number of days above nn mm (Rnnmm)"));
		climateIndexList.add(new KeyValueElement("CDD",
				"Consecutive dry days (CDD)"));
		climateIndexList.add(new KeyValueElement("CWD",
				"Consecutive wet days (CWD)"));
		climateIndexList
				.add(new KeyValueElement("R95p", "Very wet days (R95p)"));
		climateIndexList.add(new KeyValueElement("R99p",
				"Extremely wet days (R99p)"));
		climateIndexList.add(new KeyValueElement("PRCPTOT",
				"Annual total wet-day precipitation (PRCPTOT)"));
	}

	private void initStartYearList()
	{
		startYearList = new ArrayList<String>();
		for (int i = yearListStart; i <= yearListEnd; i++)
		{
			startYearList.add(String.valueOf(i)); // new String instance
		}
	}

	private void initEndYearList()
	{
		endYearList = new ArrayList<String>();
		for (int i = yearListStart; i <= yearListEnd; i++)
		{
			endYearList.add(String.valueOf(i)); // new String instance //
												// created
		}
	}

	private void initSeasonList()
	{
		seasonList = new ArrayList<KeyValueElement>();
		seasonList.add(new KeyValueElement("ANN", "Annual"));
		// seasonList.add(new KeyValueElement("DJF", "DJF"));
		// seasonList.add(new KeyValueElement("MAM", "MAM"));
		// seasonList.add(new KeyValueElement("JJA", "JJA"));
		// seasonList.add(new KeyValueElement("SON", "SON"));
		seasonList.add(new KeyValueElement("JAN", "January"));
		seasonList.add(new KeyValueElement("FEB", "February"));
		seasonList.add(new KeyValueElement("MAR", "March"));
		seasonList.add(new KeyValueElement("APR", "April"));
		seasonList.add(new KeyValueElement("MAY", "May"));
		seasonList.add(new KeyValueElement("JUN", "June"));
		seasonList.add(new KeyValueElement("JUL", "July"));
		seasonList.add(new KeyValueElement("AUG", "August"));
		seasonList.add(new KeyValueElement("SEP", "September"));
		seasonList.add(new KeyValueElement("OCT", "October"));
		seasonList.add(new KeyValueElement("NOV", "November"));
		seasonList.add(new KeyValueElement("DEC", "December"));
	}

	private void initOutputTypeList()
	{
		outputTypeList = new ArrayList<KeyValueElement>();
		outputTypeList.add(new KeyValueElement("Plot", "Plot"));
		outputTypeList.add(new KeyValueElement("ASCII", "ASCII"));
	}

	@Deprecated
	public List<KeyValueElement> getDataSourceList()
	{
		return dataSourceList;
	}

	@Deprecated
	public void setDataSourceList(List<KeyValueElement> dataSourceList)
	{
		this.dataSourceList = dataSourceList;
	}

	public List<KeyValueElement> getClimateIndexList()
	{
		return climateIndexList;
	}

	public void setClimateIndexList(List<KeyValueElement> climateIndexList)
	{
		this.climateIndexList = climateIndexList;
	}

	public List<String> getStartYearList()
	{
		return startYearList;
	}

	public void setStartYearList(List<String> startYearList)
	{
		this.startYearList = startYearList;
	}

	public List<String> getEndYearList()
	{
		return endYearList;
	}

	public void setEndYearList(List<String> endYearList)
	{
		this.endYearList = endYearList;
	}

	@Deprecated
	public int getYearListStart()
	{
		return yearListStart;
	}

	@Deprecated
	public void setYearListStart(int yearListStart)
	{
		this.yearListStart = yearListStart;
	}

	@Deprecated
	public int getYearListEnd()
	{
		return yearListEnd;
	}

	@Deprecated
	public void setYearListEnd(int yearListEnd)
	{
		this.yearListEnd = yearListEnd;
	}

	public List<KeyValueElement> getSeasonList()
	{
		return seasonList;
	}

	public void setSeasonList(List<KeyValueElement> seasonList)
	{
		this.seasonList = seasonList;
	}

	public List<KeyValueElement> getOutputTypeList()
	{
		return outputTypeList;
	}

	public void setOutputTypeList(List<KeyValueElement> outputTypeList)
	{
		this.outputTypeList = outputTypeList;
	}
}
