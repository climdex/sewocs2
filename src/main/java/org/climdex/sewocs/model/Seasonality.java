package org.climdex.sewocs.model;


public class Seasonality
{
	private String seasonalityList;
	
	public Seasonality()
	{
		initSeasonalityList();
	}
	private void initSeasonalityList()
	{
		seasonalityList = "\"FD\" : \"annualOnly\"," +
				"\"SU\" : \"annualOnly\"," +
				"\"ID\" : \"annualOnly\"," +
				"\"TR\" : \"annualOnly\"," +
				"\"GSL\" : \"annualOnly\"," +
				"\"TXx\" : \"seasonable\"," +
				"\"TNx\" : \"seasonable\"," +
				"\"TXn\" : \"seasonable\"," +
				"\"TNn\" : \"seasonable\"," +
				"\"TN10p\" : \"seasonable\"," +
				"\"TX10p\" : \"seasonable\"," +
				"\"TN90p\" : \"seasonable\"," +
				"\"TX90p\" : \"seasonable\"," +
				"\"WSDI\" : \"annualOnly\"," +
				"\"CSDI\" : \"annualOnly\"," +
				"\"DTR\" : \"seasonable\"," +
				"\"Rx1day\" : \"seasonable\"," +
				"\"Rx5day\" : \"seasonable\"," +
				"\"SDII\" : \"annualOnly\"," +
				"\"R10mm\" : \"annualOnly\"," +
				"\"R20mm\" : \"annualOnly\"," +
				"\"Rnn\" : \"annualOnly\"," +
				"\"CDD\" : \"annualOnly\"," +
				"\"CWD\" : \"annualOnly\"," +
				"\"R95p\" : \"annualOnly\"," +
				"\"R99p\" : \"annualOnly\"," +
				"\"PRCPTOT\" : \"annualOnly\"";
	}
	public String getSeasonalityList()
	{
		return seasonalityList;
	}
	public void setSeasonalityList(String seasonalityList)
	{
		this.seasonalityList = seasonalityList;
	}
}
