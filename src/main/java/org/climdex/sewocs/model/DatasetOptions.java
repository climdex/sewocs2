/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.sewocs.model;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The class holds values for options for dataSets. Values are accessed by
 * JavaScript on the map JSP page that is given this object by its action class.
 * 
 * applicationContext.xml uses the setter methods to populate the values for the
 * maps. The initial values are defined by applicationContext.xml.
 * 
 * JavaScript needs to use getXXXJSArray() methods to access the maps as JSON.
 * 
 * @author yoichi
 */
public class DatasetOptions
{
	@Autowired
	private Map<String, String> datasetNameMap;
	@Autowired
	private Map<String, String> startYearMap;
	@Autowired
	private Map<String, String> endYearMap;
	@Autowired
	private Map<String, String> layerNameMap;
	@Autowired
	private Map<String, String> styleNameMap;
	@Autowired
	private Map<String, String> dataDirMap;

	public DatasetOptions()
	{
	}

	public Map<String, String> getDatasetNameMap()
	{
		return datasetNameMap;
	}

	public void setDatasetNameMap(Map<String, String> datasetNameMap)
	{
		this.datasetNameMap = datasetNameMap;
	}

	public Map<String, String> getStartYearMap()
	{
		return startYearMap;
	}

	public void setStartYearMap(Map<String, String> startYearMap)
	{
		this.startYearMap = startYearMap;
	}

	public Map<String, String> getEndYearMap()
	{
		return endYearMap;
	}

	public void setEndYearMap(Map<String, String> endYearMap)
	{
		this.endYearMap = endYearMap;
	}

	public Map<String, String> getLayerNameMap()
	{
		return layerNameMap;
	}

	public void setLayerNameMap(Map<String, String> layerNameMap)
	{
		this.layerNameMap = layerNameMap;
	}

	public Map<String, String> getStyleNameMap()
	{
		return styleNameMap;
	}

	public void setStyleNameMap(Map<String, String> styleNameMap)
	{
		this.styleNameMap = styleNameMap;
	}

	public Map<String, String> getDataDirMap()
	{
		return dataDirMap;
	}

	public void setDataDirMap(Map<String, String> dataDirMap)
	{
		this.dataDirMap = dataDirMap;
	}

	@Autowired
	public String getDatasetNameJSArray() throws JsonGenerationException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		String datasetNameJSString = mapper.writeValueAsString(datasetNameMap);
		return datasetNameJSString;
	}

	@Autowired
	public String getStartYearJSArray() throws JsonGenerationException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		String startYearJSString = mapper.writeValueAsString(startYearMap);
		return startYearJSString;
	}

	@Autowired
	public String getEndYearJSArray() throws JsonGenerationException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		String endYearJSString = mapper.writeValueAsString(endYearMap);
		return endYearJSString;
	}

	@Autowired
	public String getLayerNameJSArray() throws JsonGenerationException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		String layerNameJSString = mapper.writeValueAsString(layerNameMap);
		return layerNameJSString;
	}

	@Autowired
	public String getStyleNameJSArray() throws JsonGenerationException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		String styleNameJSString = mapper.writeValueAsString(styleNameMap);
		return styleNameJSString;
	}
}
