/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. 
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 	
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.

 * CLIMDEX Project Group provides access to this code for bona fide
 * scientific research and personal usage only.

 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow 
 * researchers. Everyone is welcome to use the code, and we aim to make it 
 * reliable, but it does not have the same level of support as an operational 
 * service.
 */
package org.climdex.sewocs.model;
/**
 * Trivial class for key-value pair
 * The same thing can be achieved by HashMap, but construction of it
 * with key-value is more tedious.
 * @author yoichi
 *
 */
public class KeyValueElement
{
	private String key;
	private String value;

	public KeyValueElement(String key, String value)
	{
		this.key = key;
		this.value = value;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}
}
