package org.climdex.sewocs.model;

/**
 * HadEX2 station
 * This class was created in order to handle data in the HadEX2_station_list_all_INDEX_NAME.csv
 * @author yoichi
 *
 */
public class WeatherStation implements Comparable<WeatherStation>
{
	private String id; // recognized by Shapefile
	private String latitude; //recognized by Shapefile
	private String longitude ; //recognized by Shapefile
	private String altitude; //recognized by Shapefile
	private String index; // weather index name
	private String name; // station name, recognized by Shapefile
	private String country;
	private String region;
	private String sourceId;
	private String source;
	private String sourceDir;
	private String startYear;
	private String endYear;
	private String dailyData;
	private String notes;
	private String active; // whether to render the station data publically available or not.
	
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getLatitude()
	{
		return latitude;
	}

	public void setLatitude(String latitude)
	{
		this.latitude = latitude;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public String getAltitude()
	{
		return altitude;
	}

	public void setAltitude(String altitude)
	{
		this.altitude = altitude;
	}

	public String getIndex()
	{
		return index;
	}

	public void setIndex(String index)
	{
		this.index = index;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getRegion()
	{
		return region;
	}

	public void setRegion(String region)
	{
		this.region = region;
	}

	public String getSourceId()
	{
		return sourceId;
	}

	public void setSourceId(String sourceId)
	{
		this.sourceId = sourceId;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getSourceDir()
	{
		return sourceDir;
	}

	public void setSourceDir(String sourceDir)
	{
		this.sourceDir = sourceDir;
	}

	public String getStartYear()
	{
		return startYear;
	}

	public void setStartYear(String startYear)
	{
		this.startYear = startYear;
	}

	public String getEndYear()
	{
		return endYear;
	}

	public void setEndYear(String endYear)
	{
		this.endYear = endYear;
	}

	public String getDailyData()
	{
		return dailyData;
	}

	public void setDailyData(String dailyData)
	{
		this.dailyData = dailyData;
	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

	public String getActive()
	{
		return active;
	}

	public void setActive(String active)
	{
		this.active = active;
	}

	@Override
	public int compareTo(WeatherStation station)
	{
		final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    
	    if (station==null) throw new NullPointerException("cannot compare with null");
	    
	    if ( this == station ) return EQUAL;
	    
	    String thisId = this.id;
	    String thatId = station.getId();
	    
	   return thisId.compareTo(thatId); 
	}
}
