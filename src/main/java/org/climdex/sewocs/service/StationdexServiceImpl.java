package org.climdex.sewocs.service;

import org.climdex.sewocs.service.dao.DAOException;
import org.climdex.sewocs.service.dao.StationdexDAO;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service Interface implementation. It use DAO. It does not add anything new to
 * the DAO interface
 * 
 * @author yoichi
 */
public class StationdexServiceImpl implements StationdexService
{
	@Autowired
	private StationdexDAO stationdexDAO = null;

	public StationdexDAO getStationdexDAO()
	{
		return stationdexDAO;
	}

	public void setStationdexDAO(StationdexDAO stationdexDAO)
	{
		this.stationdexDAO = stationdexDAO;
	}

	@Override
	public byte[] getStationdexPlot(String dataDir, String dataSource,
			String stationId, String stationName, String climateIndex,
			String startYear, String endYear, String season, String outputType)
			throws ServiceException
	{
		byte[] byteArray = null;
		try
		{
			byteArray = this.stationdexDAO.getStationIndexPlot(dataDir,
					dataSource, stationId, stationName, climateIndex,
					startYear, endYear, season, outputType);
		}
		catch (DAOException e)
		{
			throw new ServiceException(e);
		}
		return byteArray;
	}

	public double[][] getStationdexASCII(String dataDir, String dataSource,
			String stationId, String stationName, String climateIndex,
			String startYear, String endYear, String season, String outputType)
			throws ServiceException
	{
		double[][] dd = null;
		try
		{
			dd = this.stationdexDAO.getStationdexASCII(dataDir, dataSource,
					stationId, stationName, climateIndex, startYear, endYear,
					season, outputType);
		}
		catch (DAOException e)
		{
			throw new ServiceException(e);
		}
		return dd;
	}
}
