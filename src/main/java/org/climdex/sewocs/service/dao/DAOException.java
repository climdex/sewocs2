package org.climdex.sewocs.service.dao;

public class DAOException extends Exception
{
	private static final long serialVersionUID = 142193405668360602L;

	public DAOException()
	{
		// TODO Auto-generated constructor stub
	}

	public DAOException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DAOException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DAOException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
