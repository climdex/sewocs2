/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.sewocs.service.dao;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.im4java.core.Stream2BufferedImage;
import org.im4java.process.ProcessStarter;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * DAO class to produce requested climate extreme index
 * 
 * Return type could be byte[] (BufferedImage) or double [][] (ASCII file)
 * 
 * Prerequisites: rJava (JRI), JRIEngine, REngine, RserveEngine, im4java.
 * 
 * R requires Rserve, cairoDevice and RGtk2
 * 
 * OS requires Xvfb, Gtk2+ library,Cairo library, X11 and ImageMagick
 * 
 * ImageMagick is used to add a logo to the image.
 * 
 * See README for more details
 * 
 * TODO: Use DELIM for PATh literals
 */
public class StationdexDAOImpl implements StationdexDAO
{
	private static final Logger logger = Logger
			.getLogger(StationdexDAOImpl.class);

	private static final String DELIM = File.separator;
	/*
	 * Default values, overridden by applicatinoContext.xml
	 */
	@Autowired
	private String R_SCRIPT_DIR = "/scratch/climdex/RScripts/sewocs2";
	@Autowired
	private String TIMESERIES_SCRIPT = "sewocs_functions.R";
	@Autowired
	private String TIMESERIES_FUNCTION_NAME = "plotOrASCII";
	@Autowired
	private String DISPLAY = "localhost:200.0";
	@Autowired
	private String RSERVE_HOST = "localhost";
	@Autowired
	private int RSERVE_PORT = 26311;
	@Autowired
	private String UNSW_LOGO_PATH = "/scratch/climdex/images/logo_unsw_small.png";
	@Autowired
	private String IMAGE_MAGICK_BIN = "/opt/local/bin"; // for mac os x

	// private String IMAGE_MAGICK_BIN = "/usr/bin"; // For Linux

	/**
	 * Public method to get a plot PNG as a byte[] array
	 * 
	 * @throws REXPMismatchException
	 * @throws RserveException
	 */
	@Override
	public byte[] getStationIndexPlot(String dataDir, String dataSource,
			String stationId, String stationName, String climateIndex,
			String startYear, String endYear, String season, String outputType)
			throws DAOException
	{
		byte[] byteArray = null;
		REXP r;
		// Get output
		try
		{
			r = runSewocsRFunction(dataDir, dataSource, stationId, stationName,
					climateIndex, startYear, endYear, season, outputType);

			// byteArray = rConnection.eval("output").asBytes();
			byteArray = r.asBytes();
		}
		catch (RserveException e)
		{
			throw new DAOException(e);
		}
		catch (REXPMismatchException e)
		{
			throw new DAOException(e);
		}
		catch (RException e)
		{
			throw new DAOException(e);
		}

		// Branding with a logo
		byteArray = addLogo(byteArray, UNSW_LOGO_PATH);

		return byteArray;
	}

	/**
	 * A helper method to add a logo PNG data to the plot PNG data in memory.
	 * The ImageMagick syntax has been changed.
	 * <code>convert plotTimeseries.png logo_unsw_small.png -geometry
	 * +20+555 -composite composite.png</code>
	 * 
	 * @param byteArray
	 *            byte[]
	 * @param logoPath
	 *            String
	 * @return byteArray byte[]
	 * @throws DAOException
	 */
	private byte[] addLogo(byte[] byteArray, String logoPath)
			throws DAOException
	{
		InputStream plotStream = new ByteArrayInputStream(byteArray);
		byte[] outByteArray = null;
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		BufferedImage plotBfdImage = null;
		BufferedImage compositeBfdImage = null;

		IMOperation op = new IMOperation();
		op.addImage();
		op.addImage();
		op.geometry(100, 100, 20, 555); // 100%, 100%, x+20pix, y+555pix
		op.composite();
		op.addImage("png:-"); // output: stdout

		ConvertCmd cmd = new ConvertCmd();
		/* =========================== */
		ProcessStarter.setGlobalSearchPath(this.IMAGE_MAGICK_BIN);
		// cmd.setSearchPath(this.IMAGE_MAGICK_BIN);
		/* =========================== */
		Stream2BufferedImage s2b = new Stream2BufferedImage();
		cmd.setOutputConsumer(s2b);
		try
		{
			plotBfdImage = ImageIO.read(plotStream);
			cmd.run(op, plotBfdImage, logoPath); // execute the operation
			compositeBfdImage = s2b.getImage(); // extract BufferedImage
			ImageIO.write(compositeBfdImage, "png", byteArrayOutputStream);
			byteArrayOutputStream.flush();
			outByteArray = byteArrayOutputStream.toByteArray();
		}
		catch (IOException e)
		{
			throw new DAOException(e);
		}
		catch (InterruptedException e)
		{
			throw new DAOException(e);
		}
		catch (IM4JavaException e)
		{
			throw new DAOException(e);
		}

		byteArray = outByteArray;
		return byteArray;
	}

	/**
	 * Private method to run R script with parameters
	 * 
	 * @param dataSource
	 * @param stationId
	 * @param countryId
	 * @param stationName
	 * @param climateIndex
	 * @param startYear
	 * @param endYear
	 * @param season
	 * @param outputType
	 * @return
	 * @throws RserveException
	 * @throws REXPMismatchException
	 * @throws RException
	 */
	private REXP runSewocsRFunction(String dataDir, String dataSource,
			String stationId, String stationName, String climateIndex,
			String startYear, String endYear, String season, String outputType)
			throws RserveException, REXPMismatchException, RException
	{
		String rCmd = "";
		REXP rexp = null;
		String errMsg = null;

		RConnection rConnection = new RConnection(RSERVE_HOST, RSERVE_PORT);

		String scriptPath = R_SCRIPT_DIR + DELIM + TIMESERIES_SCRIPT;
		String indexFilename = stationId + "_" + climateIndex + ".txt";
		String inputFilePath = dataDir + DELIM + climateIndex + DELIM
				+ indexFilename;
		
		try
		{
			rCmd = "Sys.setenv(\"DISPLAY\"=\"" + DISPLAY + "\")\n";
			rCmd += "source('" + scriptPath + "')\n";
			rCmd += "dataSource<-\"" + dataSource + "\"\n";
			rCmd += "outputType<-\"" + outputType + "\"\n";
			rCmd += "stationId<-\"" + stationId + "\"\n";
			rCmd += "stationName<-\"" + stationName + "\"\n";
			rCmd += "climateIndex<-\"" + climateIndex + "\"\n";
			rCmd += "startYear<-" + startYear + "\n";
			rCmd += "endYear<-" + endYear + "\n";
			rCmd += "season<-\"" + season + "\"\n";
			rCmd += "stationFile<-\"" + inputFilePath + "\"\n";
			rCmd += "output <-" + TIMESERIES_FUNCTION_NAME
					+ "(dataSource, outputType, stationId, stationName, "
					+ "climateIndex, startYear, endYear, season, stationFile)";
			rexp = tryR(rConnection, rCmd);
		}
		catch (RException e)
		{
			errMsg = e.getMessage();
			logger.error(errMsg);
			if (errMsg.contains("CGContextSet"))
			{
				; // Ignore non-critical error on Mac OS X
			}
			else
				throw e;
		}

		return rexp;
	}

	/**
	 * A utility method to evaluate an R sentence. It checks for an error
	 * 
	 * @link http://www.rforge.net/Rserve/faq.html
	 * @param rConnection
	 * @param rSentence
	 * @return
	 * @throws RException
	 * @throws RserveException
	 * @throws REXPMismatchException
	 */
	private REXP tryR(RConnection rConnection, String rSentence)
			throws RException, RserveException, REXPMismatchException
	{
		String trySentence = "try({" + rSentence + "}, silent=TRUE)";
		REXP r = rConnection.eval(trySentence);
		if (r.inherits("try-error")) throw new RException(r.asString());
		return r;
	}

	/**
	 * A public method to get output as an ASCII data
	 * 
	 * @param dataSource
	 * @param stationId
	 * @param countryId
	 * @param stationName
	 * @param climateIndex
	 * @param startYear
	 * @param endYear
	 * @param season
	 * @param outputType
	 * @return
	 * @throws DAOException
	 */
	public double[][] getStationdexASCII(String dataDir, String dataSource,
			String stationId, String stationName, String climateIndex,
			String startYear, String endYear, String season, String outputType)
			throws DAOException
	{
		double[][] dd = null;
		REXP r = null;
		try
		{
			r = runSewocsRFunction(dataDir, dataSource, stationId, stationName,
					climateIndex, startYear, endYear, season, outputType);

			dd = r.asDoubleMatrix();
		}
		catch (RserveException e)
		{
			throw new DAOException(e);
		}
		catch (REXPMismatchException e)
		{
			throw new DAOException(e);
		}
		catch (RException e)
		{
			throw new DAOException(e);
		}

		return dd;
	}

	public String getR_SCRIPT_DIR()
	{
		return R_SCRIPT_DIR;
	}

	public void setR_SCRIPT_DIR(String r_SCRIPT_DIR)
	{
		R_SCRIPT_DIR = r_SCRIPT_DIR;
	}

	public String getTIMESERIES_SCRIPT()
	{
		return TIMESERIES_SCRIPT;
	}

	public void setTIMESERIES_SCRIPT(String tIMESERIES_SCRIPT)
	{
		TIMESERIES_SCRIPT = tIMESERIES_SCRIPT;
	}

	public String getTIMESERIES_FUNCTION_NAME()
	{
		return TIMESERIES_FUNCTION_NAME;
	}

	public void setTIMESERIES_FUNCTION_NAME(String tIMESERIES_FUNCTION_NAME)
	{
		TIMESERIES_FUNCTION_NAME = tIMESERIES_FUNCTION_NAME;
	}

	public String getDISPLAY()
	{
		return DISPLAY;
	}

	public void setDISPLAY(String dISPLAY)
	{
		DISPLAY = dISPLAY;
	}

	public String getRSERVE_HOST()
	{
		return RSERVE_HOST;
	}

	public void setRSERVE_HOST(String rSERVE_HOST)
	{
		RSERVE_HOST = rSERVE_HOST;
	}

	public int getRSERVE_PORT()
	{
		return RSERVE_PORT;
	}

	public void setRSERVE_PORT(int rSERVE_PORT)
	{
		RSERVE_PORT = rSERVE_PORT;
	}

	public String getUNSW_LOGO_PATH()
	{
		return UNSW_LOGO_PATH;
	}

	public void setUNSW_LOGO_PATH(String uNSW_LOGO_PATH)
	{
		UNSW_LOGO_PATH = uNSW_LOGO_PATH;
	}

	public String getIMAGE_MAGICK_BIN()
	{
		return IMAGE_MAGICK_BIN;
	}

	public void setIMAGE_MAGICK_BIN(String iMAGE_MAGICK_BIN)
	{
		IMAGE_MAGICK_BIN = iMAGE_MAGICK_BIN;
	}
}
