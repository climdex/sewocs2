package org.climdex.sewocs.service;

/**
 * Service is a redundant facade for the DAO design pattern
 * 
 * @author yoichi *
 */

public abstract interface StationdexService
{
	abstract public byte[] getStationdexPlot(String dataDir, String dataSource,
			String stationId, String stationName, String climateIndex,
			String startYear, String endYear, String season, String outputType)
			throws ServiceException;

	abstract public double[][] getStationdexASCII(String dataDir,
			String dataSource, String stationId, String stationName,
			String climateIndex, String startYear, String endYear,
			String season, String outputType) throws ServiceException;
}
