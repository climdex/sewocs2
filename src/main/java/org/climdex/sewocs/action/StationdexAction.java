/*
 * Copyright 2010-2012 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.sewocs.action;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.climdex.sewocs.model.DatasetOptions;
import org.climdex.sewocs.model.Seasonality;
import org.climdex.sewocs.model.StationdexRequest;
import org.climdex.sewocs.service.ServiceException;
import org.climdex.sewocs.service.StationdexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.climdex.sewocs.model.StationdexOptions;

import com.opensymphony.xwork2.ActionSupport;

/**
 * An Action class to handle station index request.
 */
public class StationdexAction extends ActionSupport
{
	private static final long serialVersionUID = -8356266301680852648L;
	private static final Logger logger = Logger
			.getLogger(StationdexAction.class);
	private static final String ASCII_SUFFIX = "txt";
	private static final String ASCII_CONTENT_TYPE = "text/plain";
	private static final String PNG_SUFFIX = "png";
	private static final String PNG_CONTENT_TYPE = "image/png";

	/**
	 * request variables returned by the action form
	 */
	@Autowired
	private StationdexRequest stationdexRequest = null;
	/**
	 * DatasetOptions variables used in the action JSP page
	 */
	@Autowired
	private DatasetOptions datasetOptions;
	/**
	 * select field's options values used in the action JSP page
	 */
	@Autowired
	private StationdexOptions stationdexOptions;
	/**
	 * Seasonality select field's options values used in the action JSP page
	 */
	@Autowired
	private Seasonality seasonality;
	/**
	 * File download. This is the file stream
	 */
	@Autowired
	private InputStream fileInputStream = null;
	/**
	 * File download. This is contentType (image/png or text/plain).
	 */
	@Autowired
	private String contentType = null;
	/**
	 * File download. The download will be named as fileName.
	 */
	@Autowired
	private String fileName = null;

	/**
	 * Spring bean
	 */
	@Autowired
	private StationdexService stationdexService;

	/**
	 * Main method returns the output as a Struts download file or an error. It
	 * calls either doPlot() or doASCII().
	 */
	public String execute() throws Exception
	{
		String dataSource = this.stationdexRequest.getDataSource();
		String stationId = this.stationdexRequest.getStationId();
		String stationName = this.stationdexRequest.getStationName();
		// latitude, longitude and altitude are ignored
		String climateIndex = this.stationdexRequest.getClimateIndex();
		String startYear = this.stationdexRequest.getStartYear();
		String endYear = this.stationdexRequest.getEndYear();
		String season = this.stationdexRequest.getSeason();
		String outputType = this.stationdexRequest.getOutputType();

		Map<String, String> getDataDirMap = this.datasetOptions.getDataDirMap();
		String dataDir = (String) getDataDirMap.get(dataSource);

		logger.debug("dataDir= " + dataDir);
		logger.debug("dataSource= " + dataSource);
		logger.debug("stationId= " + stationId);
		logger.debug("stationName= " + stationName);
		logger.debug("climateIndex= " + climateIndex);
		logger.debug("startYear= " + startYear);
		logger.debug("endYear= " + endYear);
		logger.debug("season= " + season);
		logger.debug("outputType= " + outputType);

		try
		{
			if ("Plot".equals(outputType))
			{
				return doPlot(dataDir, dataSource, stationId, stationName,
						climateIndex, startYear, endYear, season, outputType);
			}
			else
				if ("ASCII".equals(outputType))
				{
					return doASCII(dataDir, dataSource, stationId, stationName,
							climateIndex, startYear, endYear, season,
							outputType);
				}
				else
				{
					addActionError("Unknown Output Type");
					return ERROR;
				}
		}
		catch (Exception e)
		{
			String msg = e.getMessage();
			String shortMsg = null;
			if (msg.contains("does not provide the data"))
			{
				shortMsg = "No data available for this index at this station";
			}
			else
				if (msg.contains("Station does not provide the data within the range"))
				{
					shortMsg = "No data available within the range";
				}
				else
				{
					logger.error(msg);
					shortMsg = "server error. Please report the error\n" + msg;
				}
			addActionError(shortMsg);
			return ERROR;
		}
	}

	/**
	 * A private method to obtain a plot as ByteArrayInputStream which is
	 * assigned to file download by Struts.
	 * 
	 * @param dataSource
	 * @param stationId
	 * @param countryId
	 * @param stationName
	 * @param climateIndex
	 * @param startYear
	 * @param endYear
	 * @param season
	 * @param outputType
	 * @return
	 * @throws ServiceException
	 */
	private String doPlot(String dataDir, String dataSource, String stationId,
	/* String countryId, */String stationName, String climateIndex,
			String startYear, String endYear, String season, String outputType)
			throws ServiceException
	{
		String downloadFileName = dataSource + "_" + stationId + "_"
				+ climateIndex + "_" + startYear + "-" + endYear + "_" + season;

		this.fileName = downloadFileName + "." + PNG_SUFFIX;
		this.contentType = PNG_CONTENT_TYPE;

		byte[] bytes = null;
		bytes = this.stationdexService.getStationdexPlot(dataDir, dataSource,
				stationId, stationName, climateIndex, startYear, endYear,
				season, outputType);

		// R throws Exception when null is returned, so this is not executed
		if (bytes == null || bytes.length == 0)
		{
			addActionError("No result was returned");
			return ERROR;
		}
		/**
		 * This code relies on the fact that the size of the png is not bigger
		 * than the available memory
		 */
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		BufferedInputStream bis = new BufferedInputStream(bais);
		this.fileInputStream = bis;
		return NONE; // no view (jsp) to be shown
	}

	/**
	 * A private method to return ASCII file as a ByteArrayInputStream which
	 * contains printed out characters as String.
	 * 
	 * @param dataSource
	 * @param stationId
	 * @param countryId
	 * @param stationName
	 * @param climateIndex
	 * @param startYear
	 * @param endYear
	 * @param season
	 * @param outputType
	 * @return
	 * @throws ServiceException
	 */
	private String doASCII(String dataDir, String dataSource, String stationId,
			String stationName, String climateIndex, String startYear,
			String endYear, String season, String outputType)
			throws ServiceException
	{
		String downloadFileName = dataSource + "_" + stationId + "_"
				+ climateIndex + "_" + startYear + "-" + endYear + "_" + season;

		this.fileName = downloadFileName + "." + ASCII_SUFFIX;
		this.contentType = ASCII_CONTENT_TYPE;

		double[][] dd = null;
		dd = this.stationdexService.getStationdexASCII(dataDir, dataSource,
				stationId, stationName, climateIndex, startYear, endYear,
				season, outputType);

		// R throws Exception when null is returned, so this is not executed
		if (dd == null || dd.length == 0)
		{
			addActionError("No result was returned");
			return ERROR;
		}

		/**
		 * This code relies on the fact that the size of the ASCII data is not
		 * bigger than the available memory
		 */
		Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy-MM-dd G HH:mm:ss z Z");
		String todayStr = formatter.format(today);
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		// Add a header line for the ASCII file data
		String headerFormat = "Data calculated on %sGMT for Station %s of dataset %s, \u00a9 CLIMDEX (www.climdex.org)\n";
		pw.format(headerFormat, todayStr, stationId, dataSource);
		String name = climateIndex + "-" + season;
		pw.format("year %s\n", name);
		/**
		 * x[1] < -99.8 is tested, because R's NA is passed as -99.9. rJava does
		 * not translate it correctly to Java's NaN.
		 */
		for (double[] x : dd)
		{
			if (Double.isNaN(x[1]) || x[1] < -99.8)
				pw.format("%4.0f      -99.9\n", x[0]);
			else
				pw.format("%4.0f %10.1f\n", x[0], x[1]);
		}
		pw.flush();
		pw.close();

		String formattedString = sw.toString();
		byte[] bytes = formattedString.getBytes(Charset.forName("UTF-8"));
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		BufferedInputStream bis = new BufferedInputStream(bais);
		this.fileInputStream = bis;
		return NONE; // no view (jsp) to be shown
	}

	/**
	 * The method is called to initialize the beans for the first time.
	 * 
	 * @return
	 */
	public String display()
	{
		return SUCCESS;
	}

	public StationdexRequest getStationdexRequest()
	{
		return stationdexRequest;
	}

	public void setStationdexRequest(StationdexRequest stationdexRequest)
	{
		this.stationdexRequest = stationdexRequest;
	}

	public StationdexOptions getStationdexOptions()
	{
		return stationdexOptions;
	}

	public void setStationdexOptions(StationdexOptions stationdexOptions)
	{
		this.stationdexOptions = stationdexOptions;
	}

	public Seasonality getSeasonality()
	{
		return seasonality;
	}

	public void setSeasonality(Seasonality seasonality)
	{
		this.seasonality = seasonality;
	}

	public InputStream getFileInputStream()
	{
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream)
	{
		this.fileInputStream = fileInputStream;
	}

	public String getContentType()
	{
		return contentType;
	}

	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public StationdexService getStationdexService()
	{
		return stationdexService;
	}

	public void setStationdexService(StationdexService stationdexService)
	{
		this.stationdexService = stationdexService;
	}

	public DatasetOptions getDatasetOptions()
	{
		return datasetOptions;
	}

	public void setDatasetOptions(DatasetOptions datasetOptions)
	{
		this.datasetOptions = datasetOptions;
	}
}
