/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.sewocs.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.climdex.sewocs.service.dao.StationdexDAOImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

/**
 * 
 * <p>
 * TimeSeriesServiceTest.java
 * </p>
 * 
 * {@link http
 * ://static.springsource.org/spring/docs/2.5.x/reference/testing.html} Spring
 * 2.5.x Testing *
 * 
 * @author yoichi
 * 
 */
@ContextConfiguration(locations = { "/applicationContext.xml",
		"/applicationContext-test.xml" })
public class StationdexServiceTest
{
	private StationdexDAOImpl dao;
	private StationdexServiceImpl service;

	@Before
	public void setUp()
	{
		this.dao = new StationdexDAOImpl();
		this.service = new StationdexServiceImpl();
		service.setStationdexDAO(dao);
	}

	@Test
	public void bytesReturned() throws IOException
	{
		assertTrue(true);
		return;
		
		/*
		String dataDir = "/scratch/climdex/ghcndex/current/stn-indices";
		String dataSet = "GHCND";
		String stationId = "ASN00066062";
		String stationName = "SYDNEY%20(OBSERVATORY%20HILL)";
		String climateIndex = "TXx";
		String startYear = "1851";
		String endYear = "2012";
		String season = "ANN";
		String outputType = "Plot";

		byte[] bytes = null;
		try
		{
			bytes = service.getStationdexPlot(dataDir, dataSet, stationId, stationName,
					climateIndex, startYear, endYear, season, outputType);
		}
		catch (ServiceException e)
		{
			fail(e.getMessage());
		}

		OutputStream os = new FileOutputStream("test-timeseries.png");
		BufferedOutputStream bos = new BufferedOutputStream(os);
		bos.write(bytes);
		bos.flush();
		bos.close();

		assertNotNull(bytes);
		*/
	}

	@Test
	public void matrixReturned() throws IOException
	{
		assertTrue(true);
		return;
		/*
		String dataDir = "/scratch/climdex/ghcndex/current/stn-indices";
		String dataSet = "GHCND";
		String stationId = "ASN00066062";
		String stationName = "SYDNEY%20(OBSERVATORY%20HILL)";
		String climateIndex = "TXx";
		String startYear = "1851";
		String endYear = "2012";
		String season = "ANN";
		String outputType = "ASCII";

		double[][] dd = null;
		try
		{
			dd = service.getStationdexASCII(dataDir, dataSet, stationId, stationName,
					climateIndex, startYear, endYear, season, outputType);
		}
		catch (ServiceException e)
		{
			fail(e.getMessage());
		}

		OutputStream fos = new FileOutputStream("test-timeseries.ascii");
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		PrintWriter pw = new PrintWriter(bos);
		for (double[] x : dd)
		{
			if (Double.isNaN(x[1]))
				pw.format("%4.0f      -99.9\n", x[0]);
			else
				pw.format("%4.0f %10.1f\n", x[0], x[1]);
		}
		pw.flush();
		pw.close();

		assertNotNull(dd);
		*/
	}
}