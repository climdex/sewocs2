/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.sewocs.im4java;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;
import org.im4java.core.Stream2BufferedImage;

/**
 * <p>
 * Test for creating a composite image with an ImageMagick library, im4java
 * </p>
 * <p>
 * This is equivalent of:<br/>
 * <code>convert plotTimeseries.png logo_unsw_small.png \ <br/>
 * -geometry +20+555 -composite composite.png</code>
 * </p>
 * @see http://im4java.sourceforge.net/docs/dev-guide.html#bufferedImages
 * @author yoichi2
 * 
 */
public class BrandWithLogo
{
	public static void main(String[] args)
	{
		File plotFile = new File(
				"/Users/yoichi/Documents/workspace/sewocs/Resources/plotTimeseries.png");

		File logoFile = new File(
				"/Users/yoichi/Documents/workspace/sewocs/Resources/logo_unsw_small.png");

		File outFile = new File(
				"/Users/yoichi/Documents/workspace/sewocs/Resources/composite.png");

		ConvertCmd cmd = new ConvertCmd();
		// String imPath="/usr/lib64/ImageMagick-6.2.8/";
		String imPath = "/opt/local/bin";

		cmd.setSearchPath(imPath);

		IMOperation op = new IMOperation();
		op.addImage();
		op.addImage();
		op.geometry(100, 100, 20, 555); // 100%, 100%, x+20pix, y+555pix
		op.composite();
		op.addImage("png:-"); // output: stdout

		Stream2BufferedImage s2b = new Stream2BufferedImage();
		cmd.setOutputConsumer(s2b);

		BufferedImage plotBfImage = null;
		BufferedImage logoBfImage = null;
		BufferedImage compositeBfImage = null;
		try
		{
			plotBfImage = ImageIO.read(plotFile);
			logoBfImage = ImageIO.read(logoFile);
			// execute the operation
			cmd.run(op, plotBfImage, logoBfImage);
			// extract BufferedImage from OutputConsumer
			compositeBfImage = s2b.getImage();

			ImageIO.write(compositeBfImage, "png", outFile);
			// outByteArray = out.toByteArray();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// InputStream in = new ByteArrayInputStream(byteArray);
	// byte[] outByteArray = null;
	// ByteArrayOutputStream out = new ByteArrayOutputStream();
	// BufferedImage inBufferedImage = null;
	// BufferedImage outBufferedImage = null;
	// byteArray = outByteArray;

}
