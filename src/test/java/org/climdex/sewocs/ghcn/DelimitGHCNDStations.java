/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. 
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 	
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.

 * CLIMDEX Project Group provides access to this code for bona fide
 * scientific research and personal usage only.

 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow 
 * researchers. Everyone is welcome to use the code, and we aim to make it 
 * reliable, but it does not have the same level of support as an operational 
 * service.
 */
package org.climdex.sewocs.ghcn;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * <P>
 * DelimitGHCNDStations.java
 * </p>
 * <p>
 * This class reads the GHCN-Daily station data text file, and convert it to a
 * file with a header and each data field delimited with TAB character.
 * <P>
 * -999 is replaced as an empty field. Unused fields are removed. This is to
 * put it in the dame format that the other dataSet files are in.
 * </P>
 * <P>
 * The file is suitable to be used for QGIS' tab-delimited Text input in order
 * to create a map layer and saved as Shapefile.
 * </P>
 * 
 * @author yoichi2
 * 
 */
public class DelimitGHCNDStations
{
	private final static String DELIM = "\t";
	private final static String LF = "\n";
	private static final String ACTIVE = "1"; // All station data are available

	public static void main(String[] args)
	{
		File stationFile = new File(
				"/scratch/climdex/ghcndex/current/all-stations.txt");
		FileReader fr = null;
		BufferedReader br = null;
		File delimtedFile = new File(
				"/scratch/climdex/ghcndex/current/all-stations.tab");
		if (delimtedFile.exists()) delimtedFile.delete();
		FileWriter fw = null;
		BufferedWriter bw = null;
		try
		{
			fr = new FileReader(stationFile);
			br = new BufferedReader(fr);

			fw = new FileWriter(delimtedFile);
			bw = new BufferedWriter(fw);

			String stationId, countryCode, latitude, longitude, altitude;
			String usState, stationName, gsn, hcn, wmoid;
			String line = null;
			String outline = null;
			int i = 0;
			while ((line = br.readLine()) != null)
			{
				i++;
				System.out.println("line=" + i);

				if (i == 1)
				{
					outline = "ID\tLATITUDE\tLONGITUDE\tALTITUDE\tNAME\tACTIVE\n";
				}
				else
				{
					stationId = line.substring(0, 11).trim(); // index 0 to 10
					countryCode = line.substring(0, 2).trim(); // index 0 & 1
					latitude = line.substring(12, 20).trim(); // index 12 to 19
					longitude = line.substring(21, 30).trim();
					altitude = line.substring(31, 37).trim();
					usState = line.substring(38, 40).trim();
					stationName = line.substring(41, 71).trim();
					gsn = line.substring(72, 75).trim();
					hcn = line.substring(76, 79).trim();
					wmoid = line.substring(80, 85).trim();

					if (latitude.contains("-999")) latitude = "";
					if (longitude.contains("-999")) longitude = "";
					if (altitude.contains("-999")) altitude = "";

					outline = stationId + DELIM;
					outline += latitude + DELIM;
					outline += longitude + DELIM;
					outline += altitude + DELIM;
					outline += stationName + DELIM;
					outline += ACTIVE + LF;
				}

				bw.write(outline);
				bw.flush();
			}
			bw.flush();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (br != null) try
			{
				br.close();
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (bw != null) try
			{
				bw.close();
			}
			catch (IOException e2)
			{
				e2.printStackTrace();
			}
		}

	}

}
