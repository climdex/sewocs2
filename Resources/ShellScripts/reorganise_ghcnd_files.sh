#!/bin/bash

# Run the untar script first, which creates INDEXID directories.
# Each direoctry holds all the stations that produced the index data.
# The files are names as e.g. ${STNID}_${INDEXID}.txt
# The CONTRYID is the first 3 letters of the STNID

cd /scratch/climdex/ghcndex/GHCNDEX_2012/ghcndex_Oct2012/stn-indices
DIRS=*
for D in $DIRS
do
	echo "cd $D"
	cd $D
	FILES=*
	for F in $FILES
	do 	
		echo "Processing $D/$F"
		NAME=${F%.txt}
		STNID=${NAME%[_]*}
		CNTRYID=${STNID:0:2}
		INDEXID=${NAME#*[_]}
		echo "  NAME = $NAME"
		echo "  STNID = $STNID"
		echo "  CNTRYID = $CNTRYID"
		echo "  INDEXID = $INDEXID"
		STNDIR=/scratch/climdex/ghcndex/GHCNDEX_2012/ghcndex_Oct2012/stn-indices/by_contry/$CNTRYID/$STNID
		if [ ! -d "$STNDIR" ]; then
			echo "  mkdir $STNDIR"
			mkdir -p "$STNDIR"
		fi
		echo "  mv $F	$STNDIR/"
		mv $F $STNDIR/
	done
	cd ..
done
